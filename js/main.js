$(document).ready(function(){
	var params = window
	    .location
	    .search
	    .replace('?','')
	    .split('&')
	    .reduce(
	        function(p,e){
	            var a = e.split('=');
	            p[ decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
	            return p;
	        },
	        {}
	    );

	if(params['get']){
		var url = "/api/get.php?get="+params['get'];
	}else{		
		var url = "/api/get.php";
	}

	$.ajax({
	  dataType: "json",
	  url: url,
	  data: '',
	  success: success
	});
});

function success(data){
	console.log(data);
	if(data.ERROR){
		$("body").html(data.ERROR);
	}

	if(data.MASS){		
	  var items = [];
	  $("body").html("<h1>Просмотренные ранее</h1>");
	  $.each( data.MASS, function( key, val ) {
	    items.push( "<li id='" + key + "'><a href='/?get=" + val.DATA + "'>" + val.DATA + "</a></li>" );
	  });
	 
	  $( "<ul/>", {
	    "class": "my-new-list",
	    html: items.join( "" )
	  }).appendTo( "body" );
	}

	if(data.OBJ){
		  var item = JSON.parse(data.OBJ.JSON);

		  var items = [];
	  	  $("body").html("<h1>По запросу из базы</h1>");
		  $.each( item.Valute, function( key, val ) {
		  	console.log(val);
		    items.push( "<li id='" + key + "'>" + val.CharCode + " = " + val.Value + " рублей за " + val.Nominal + "</li>" );
		  });
		 
		  $( "<ul/>", {
		    "class": "my-new-list",
		    html: items.join( "" )
		  }).appendTo( "body" );

	}

	if(data.GET){
			
		  var items = [];
	  	  $("body").html("<h1>По запросу из api</h1>");
		  $.each( data.GET.Valute, function( key, val ) {
		  	console.log(val);
		    items.push( "<li id='" + key + "'>" + val.CharCode + " = " + val.Value + " рублей за " + val.Nominal + "</li>" );
		  });
		 
		  $( "<ul/>", {
		    "class": "my-new-list",
		    html: items.join( "" )
		  }).appendTo( "body" );

	}

}