<?php
namespace api;
use PDO;

class bdconnect{

	const dbhost = "127.0.0.1"; // ХОСТ
	const dbname = "testcbr"; // название базы данных
	const dblogin = "root"; // пользователь базы данных
	const dbpassword = ""; // пароль базы данных

	function __construct(){ 
		// функция работает при объявлении класса
	}

	/**
	* метод подключения к БД
	* @param $sql - SQL запрос
	* @return возращает массив данных из БД
	*/
	public static function connect($sql){


		if(empty(self::dbhost)){
			echo "<span style=\"color:red;font-family:arial\">Отсутствует указание хоста в настройках сайта</span> <br/>";
			$error = 1;
		}
		if(empty(self::dbname)){
			echo "<span style=\"color:red;font-family:arial\">Отсутствует название базы данных в настройках сайта</span> <br/>";
			$error = 1;
		}
		if(empty(self::dblogin)){
			echo "<span style=\"color:red;font-family:arial\">Отсутствует логин пользователя базы данных в настройках сайта</span> <br/>";
			$error = 1;
		}
		if(!empty($error)){
			echo "<span style=\"color:red;font-family:arial\">Критическая ошибка! Невозможно подключиться к базе данных.</span> <br/>";
			die();
		}else{
		try {
    		$pdo = new PDO('mysql:host='.self::dbhost.';dbname='.self::dbname, self::dblogin, self::dbpassword);
    		if($sql){    		
	    		$stmt = $pdo->query($sql);
	    		if($stmt){
    				$return = array();
					while ($row = $stmt->fetch())
					{
    			 		$return[] = $row;
					}
					return $return;
				}else{
    				return false;
				}
    		}else{
    			return false;
    		}
    		$dbh = null;
		} catch (PDOException $e) {
    		print "Error!: " . $e->getMessage() . "<br/>";
			echo "<span style=\"color:red;font-family:arial\">Критическая ошибка! Невозможно подключиться к базе данных.</span> <br/>";
    		die();
		}
		}
	}

	/**
	* метод выбирает записи в таблице БД
	* @param $filter - фильтр
	* @param $select - выбор полей
	* @param $table - таблица
	* @param $order - сортировка
	* @param $cnt - ограничивает количество
	* @return возращает таблицу в соответствии с параметрами
	*/
	public static function fetch($filter,$select,$table,$page = false,$cnt = false){
		$sql = "SELECT " . $select . " from " . $table;
		if(!empty($filter)){
			$sql .= " where " . $filter;
		}		
		if(!empty($order)){
			$sql .= " " . $order;
		}
		if(!empty($page)){
			$sql .= " limit ".$page;
		}		
		return self::connect($sql);
	}

	
	/**
	* метод обновляет записи в таблице БД
	* @param $table - название таблицы
	* @param $params - параметры необходимые для обновления VOTES_CNT=10,VOTE_SUM=50
	* @param $where - условие ID=1
	* @return возращает таблицу в соответствии с параметрами
	*/
	public static function update($table,$params,$where = false){
		$sql = "UPDATE ".$table." SET ".$params." WHERE ".$where;
		return self::connect($sql);
	}

	/**
	* метод добавляет записи в таблице БД
	* @param $table - название таблицы
	* @param $params - Название полей через запятую
	* @param $insert - добавляемые параметры через запятую
	* @return возращает таблицу в соответствии с параметрами
	*/
	public static function insert($table,$params,$insert){
		$sql = "INSERT INTO ".$table." (".$params.") VALUES (".$insert.")";
		return self::connect($sql);
	}
}
?>