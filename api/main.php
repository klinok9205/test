<?php
namespace api;
include($_SERVER["DOCUMENT_ROOT"]."/api/db.php");
use api\bdconnect as BD;

header('Content-type: text/html; charset=UTF-8');

class api{

    /**
    * метод получает/добавляет записи в БД
    * @param $data - дата в формате ДД/ММ/ГГГГ
    * @return array/json возращает таблицу в соответствии с параметрами
    */
    public static function get($data = ""){
        if($data){
            if (!preg_match("/(\d{2}\/\d{2}\/\d{4})/", $data)) {
                echo json_encode(array("ERROR"=>"<h1 class='error'>Дата введена не в верном формате!</h1>"));
                return false;
            }

            $elem["OBJ"] = BD::fetch("DATA = '".$data."'","ID,DATA,JSON","kurs")[0];
            if($elem["OBJ"] && !is_null($elem["OBJ"])){
                echo json_encode($elem); // найден в базе
            }else{
                $url = "http://www.cbr.ru/scripts/XML_daily.asp?date_req={$data}";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $output = simplexml_load_string(curl_exec($ch));
                curl_close($ch);  
                $json = json_encode($output);
                $array["GET"] = json_decode($json,TRUE);
                if(strripos($array["GET"][0],"Error") !== false){
                    echo json_encode(array("ERROR"=>"<h1 class='error'>Дата введена не в верном формате!</h1>"));
                    return false;                    
                }
                print_r(json_encode($array)); // выводим
                if($data && $json){
                    BD::insert("kurs","DATA, JSON","'{$data}','{$json}'"); // записываем данные в таблицу БД 
                }
            }
        }else{ 
            $elem["MASS"] = BD::fetch("","ID,DATA","kurs");
            echo json_encode($elem);
        }
        return true;
    }

}

?>